﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Kukul
{
    public class UiManager : MonoBehaviour
    {
        public event Action StartGame;

        public GameObject ui_startPanel { get; private set; }
        public GameObject ui_CountDown { get; private set; }

        private void Awake()
        {
            ui_startPanel = transform.Find("Panel Start").gameObject;
            ui_CountDown = transform.Find("Panel Count Down").gameObject;

        }

        void Update()
        {

        }

        public void OnStartGame()
        {
            if (StartGame != null)
                StartGame?.Invoke();
        }
    }
}