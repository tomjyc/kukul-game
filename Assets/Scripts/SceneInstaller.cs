﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Kukul
{
    public class SceneInstaller : MonoInstaller<SceneInstaller>
    {
        public GameObject pref_clicker;

        public override void InstallBindings()
        {
            Container.BindFactory<RectClicker, RectClicker.Factory>().FromComponentInNewPrefab(pref_clicker);
            Container.BindInterfacesAndSelfTo<GameController>().AsSingle();
        }
    }
}
