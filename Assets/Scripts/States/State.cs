﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Kukul
{
    public abstract class State
    {
        public abstract void EnterState();
        public abstract void ExitState();
        public abstract void UpdateState();
        public abstract void ToNextState();
    }
}