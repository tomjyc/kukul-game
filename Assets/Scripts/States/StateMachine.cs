﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Kukul
{
    public class StateMachine 
    {
        public State State { get; }
        protected State state;

        public void SetState(State state)
        {

            this.state = state;
            this.state.EnterState();
        }
    }
}