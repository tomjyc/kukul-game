﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Task.Kukul
{
    public class CountDownTimer
    {
        Text text_time;

        float currentTime = 0;
        float startTime = 5;

        public CountDownTimer(Text text_time)
        {
            this.text_time = text_time;
            currentTime = startTime;
        }

        public void Update()
        {
            currentTime -= 1 * Time.deltaTime;
            text_time.text = currentTime.ToString("0");
        }

        public float getCurrentTime()
        {
            return currentTime;
        }
    }

    public class CountDown : State
    {
        private GameController gameController;
        CountDownTimer countDownTimer;

        UiManager uiManager;
        Text text_timer;

        public CountDown(GameController gameController)
        {
            this.gameController = gameController;
            uiManager = this.gameController.uiManager;
            text_timer = uiManager.ui_CountDown.transform.GetChild(0).GetComponent<Text>();

            countDownTimer = new CountDownTimer(text_timer);
        }

        public override void EnterState()
        {
            uiManager.ui_CountDown.SetActive(true);
        }

        public override void ExitState()
        {
            uiManager.ui_CountDown.SetActive(false);
        }

        public override void ToNextState()
        {
            ExitState();
            gameController.SetState(new GamePlay(gameController));
        }

        public override void UpdateState()
        {
            countDownTimer.Update();

            if (countDownTimer.getCurrentTime() <= 0)
                ToNextState();
        }
    }
}