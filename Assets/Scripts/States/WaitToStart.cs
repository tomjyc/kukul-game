﻿using UnityEngine;
using UnityEngine.UI;

namespace Task.Kukul
{
    public class WaitToStart : State
    {
        UiManager uiManager;
        GameController gameController;

        public WaitToStart(GameController gameController)
        {
            Initialize(gameController);
        }
        public WaitToStart(GameController gameController, string clickTime)
        {
            Initialize(gameController);
            uiManager.ui_startPanel.transform.Find("Text").GetComponent<Text>().text = clickTime;
        }

        void Initialize(GameController gameController)
        {
            this.gameController = gameController;
            uiManager = this.gameController.uiManager;
            uiManager.StartGame += ToNextState;
        }

        public override void EnterState()
        {
            uiManager.ui_startPanel.SetActive(true);
        }

        public override void ExitState()
        {
            uiManager.ui_startPanel.SetActive(false);
        }

        public override void ToNextState()
        {
            ExitState();
            gameController.SetState(new CountDown(gameController));
        }

        public override void UpdateState()
        {

        }
    }
}
