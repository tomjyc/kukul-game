﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Kukul
{
    public class GameTimer
    {
        float startTime;
        float currentTime;

        public GameTimer()
        {
            startTime = Time.time;
        }

        public void Update()
        {
            currentTime = Time.time - startTime;
        }

        public string GetTimeResult()
        {
            string minutes = ((int)currentTime / 60).ToString();
            string seconds = (currentTime % 60).ToString("f2");

            return minutes + ":" + seconds;
        }
    }

    public class GamePlay : State
    {
        private GameController gameController;
        GameTimer gameTimer;
        RectClicker clicker;

        public GamePlay(GameController gameController)
        {
            this.gameController = gameController;
            gameTimer = new GameTimer();
        }

        public override void EnterState()
        {
            clicker = gameController.clickerFactory.Create();
            clicker.WasClick += WasClick;
        }

        public override void ExitState()
        {
            clicker.Destroy();
        }

        public override void ToNextState()
        {
            ExitState();
            gameController.SetState(new WaitToStart(gameController, gameTimer.GetTimeResult()));
        }

        public override void UpdateState()
        {
            gameTimer.Update();
        }

        void WasClick()
        {
            ToNextState();
        }
    }
}