﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Kukul
{
    public class GameController : StateMachine, IInitializable, ITickable
    {
        [Inject] public UiManager uiManager { get; private set; }
        [Inject] public RectClicker.Factory clickerFactory { get; private set; }

        public void Initialize()
        {
            SetState(new WaitToStart(this));
        }

        public void Tick()
        {
            state.UpdateState();
        }

    }
}