﻿using System;
using UnityEngine;
using Zenject;

namespace Task.Kukul
{
    public class RectClicker : MonoBehaviour
    {
        public event Action WasClick;
        SpriteRenderer sprite;

        void Start()
        {
            sprite = GetComponent<SpriteRenderer>();
            float min = Mathf.Min(Screen.width, Screen.height);
            float size = Mathf.Lerp(0, 100, 0.1f);
            sprite.size = new Vector2(size, size);

            float spawnY = UnityEngine.Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, size / 2)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height - size / 2)).y);
            float spawnX = UnityEngine.Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(size / 2, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(Screen.width - size / 2, 0)).x);

            transform.position = new Vector2(spawnX, spawnY);
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (WasClick != null)
                    WasClick?.Invoke();
            }
        }

        internal void Destroy()
        {
            Destroy(gameObject);
        }

        public class Factory : PlaceholderFactory<RectClicker> { }
    }
}